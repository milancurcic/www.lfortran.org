---
title: Download
comments: false
date: 2021-04-17
type: page
layout: downloadlayout
---

# Git Repository

To develop LFortran, one has to install from the main git repository:

https://gitlab.com/lfortran/lfortran

When installing from git, in addition to the dependencies for the source
tarballs, one has to also install Java and ANTLR4. The souce tarballs do not
depend on Java or ANTLR4, and so they are the recommended way to install
LFortran for end users. For more details, see the [installation instructions]
in our Documentation.

[installation instructions]: https://docs.lfortran.org/installation/
